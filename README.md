Grab your lunch and bring your laptop for a 30-45 min Lunch & Learn.

This session we will “Create a REST API with Docker & Postman”.

**Objective**: Query your new API and retrieve data from it.   
**Stack**: Node, Docker   
**Tools**: Postman   

**Perquisites**:   
Download and Install Postman: https://www.getpostman.com/   
Download and Install Docker: https://www.docker.com/products/docker-desktop   
Download and Install Node: https://nodejs.org/en/   
Download and Unzip BoilerPlate: https://gitlab.com/unstephenk/lunch-and-learn-rest-api

**Optional Reads**:   
Postman: https://learning.getpostman.com/docs/postman/launching-postman/sending-the-first-request/   
Docker: https://docs.docker.com/get-started/   
Node: https://nodejs.org/en/about/   

### Installation (LOCAL ONLY)
Use these commands to run the project locally without the use of Docker:
```
npm install
npm run start
```

### Using Docker
Build the Docker container
```
docker build -t lunchandlearn .
```
Run the Docker Container
```
docker run --name lunchandlearn -p 4000:4000 -d lunchandlearn
```
Check the logs of the docker container
```
docker logs -f lunchandlearn
```

### From Postman
Open Postman, open a new tab, verify GET is selected and type in the following url:
```
http://localhost:4000/spacee
```

### From the terminal
Type this in the URL
```
curl http://localhost:4000/spacee
```

### From the Browser
Type this in the URL:
```
http://localhost:4000/spacee
```

## Cleanup
```
docker stop $(docker ps -a -q) && docker rm $(docker ps -a -q)
```