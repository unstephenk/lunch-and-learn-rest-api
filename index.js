const express = require('express');
const app = express();

const port = 4000;

// URL route
app.get("/spacee",function(request,response)
{
  console.log('Request received!');
    response.json({
      "Message":"Welcome to Node js"
    });
});

// Setup the server
app.listen(port, function () {
    const datetime = new Date();

    const message = "Server runnning on Port:- " + port + " Started at :- " + datetime;

    console.log(message);
});
